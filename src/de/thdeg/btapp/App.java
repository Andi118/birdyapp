package de.thdeg.btapp;

import de.thdeg.btapp.bluetooth.Bluetooth;
import de.thdeg.btapp.elements.BTDevice;
import de.thdeg.btapp.elements.LoggerElement;
import de.thdeg.btapp.elements.OutputElement;
import de.thdeg.btapp.elements.TopBarElement;
import javafx.application.Application;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.Scene;
import javafx.scene.control.TextArea;
import javafx.scene.layout.BorderPane;
import javafx.stage.Stage;

public class App extends Application {

    private Bluetooth bt;

    private final TextArea logger = new TextArea();
    private final TextArea output = new TextArea();

    private final ObservableList<BTDevice> dummyList = FXCollections.observableArrayList();

    public float[] positions = new float[4];

    public void start(Stage stage){
        this.bt = new Bluetooth(output, dummyList);
        logger.setEditable(false);
        output.setEditable(false);

        BorderPane root = new BorderPane();

        root.setTop(new TopBarElement(this)); // connect bar
        root.setCenter(new OutputElement(this, output));  // output
        root.setBottom(new LoggerElement(this, logger)); // logger

        stage.setScene(new Scene(root, 600, 600));
        stage.setTitle("BirdyApp");
        stage.show();

        // Thread bullshit, Kenn ich mich mit Threads aus???
        InputReader inputReader = new InputReader(this);
        inputReader.start();

        stage.setOnCloseRequest((e)-> inputReader.shutDown());
    }

    public void setOutputText(String text){
        output.setText(text);
    }

    public TextArea getLogger(){
        return logger;
    }

    public Bluetooth getBt() {
        return bt;
    }

    public static void main(String[] args) {
        launch(args);
    }

}
