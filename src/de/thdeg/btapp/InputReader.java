package de.thdeg.btapp;

import java.io.IOException;
import java.util.Arrays;

public class InputReader extends Thread {

    private volatile boolean running = true;
    private final App app;

    private final float[][] sensorData = new float[4][4];
    private double[] erg_point = new double[3];
    private byte[] command = {0x44};

    public InputReader(App app){
        this.app = app;
    }

    @Override
    public void run() {
        while(running){
            if(app.getBt().is != null && app.getBt().isConnected){
                app.getBt().readFloatArray(app.positions);
                calculation();
                StringBuilder msg  = new StringBuilder("OUTPUT: " + "POS: " + app.positions[0]+ " \n");
                for(double f : erg_point){
                    msg.append(f).append(", ");
                }
                msg.append("\n");
                app.setOutputText(msg.toString());
            }
            try {
                Thread.sleep(1000);
                if(app.getBt().isConnected)app.getBt().write(command);
            } catch (InterruptedException | IOException e) {
                e.printStackTrace();
            }
        }
    }

    private void calculation(){
        assignData(app.positions[1], app.positions[2], app.positions[3], (int)app.positions[0]);
        // experiment, single or nested... it does NOT matter for our purposes !!!
        for(int i = 0; i < 3; i++){
            for(int j = i + 1; j < 4; j++){
                if(sensorData[i][0] != 0 && sensorData[j][0] != 0){
                    erg_point = calculate3DPoint(sensorData[i], sensorData[j]);
                    Arrays.fill(sensorData[i], 0);
                    Arrays.fill(sensorData[j], 0);
                    break;
                }
            }
        }
    }

    private void assignData(float x, float y, float z, int sensor){
        if(sensor == 0) return;
        sensorData[sensor - 1][0] = x;
        sensorData[sensor - 1][1] = y;
        sensorData[sensor - 1][2] = z;
    }

    private double vektorBetrag(float[] point1_data){
        double a = point1_data[0] * point1_data[0];
        double b = point1_data[1] * point1_data[1];
        double c = point1_data[2] * point1_data[2];
        return Math.sqrt(a + b + c);
    }

    private double[] calculate3DPoint(float[] point1_data, float[]point2_data){
        double zwischenWinkel, laengeVektor;
        double[] vec1 = new double[3];
        double[] vec2 = new double[3];
        double[] erg_vek = new double[3];

        double test = (vektorBetrag(point1_data) * vektorBetrag(point2_data));
        zwischenWinkel = (point1_data[0] * point2_data[0] + point1_data[1] * point2_data[1] + point1_data[2] * point2_data[2]) / test;
        zwischenWinkel = Math.acos(zwischenWinkel);
        laengeVektor = 1 / Math.sin(zwischenWinkel/2);

        for(int i = 0; i < 3; i++){
            vec1[i] = (point1_data[i] * laengeVektor) / vektorBetrag(point1_data);
            vec2[i] = (point2_data[i] * laengeVektor) / vektorBetrag(point2_data);
        }

        for(int i = 0; i < 3; i++){
            erg_vek[i] = 0.5 * (vec1[i] + vec2[i]);
        }

        return erg_vek;
    }

    public void shutDown(){
        running = false;
    }
}
