package de.thdeg.btapp.elements;

import de.thdeg.btapp.App;
import javafx.scene.control.TextArea;
import javafx.scene.layout.HBox;

public class OutputElement extends HBox {

    private final App app;
    private final TextArea output;

    public OutputElement(App app, TextArea output){
        this.app = app;
        this.output = output;
        output.setPrefWidth(600);
        createOutputElements();

        getChildren().add(output);
    }

    private void createOutputElements(){
        // TODO: for later usage
    }
}
