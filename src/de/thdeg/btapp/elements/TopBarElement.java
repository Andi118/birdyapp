package de.thdeg.btapp.elements;

import de.thdeg.btapp.App;
import javafx.event.ActionEvent;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;

import java.io.IOException;

public class TopBarElement extends VBox {

    private final App app;

    public TopBarElement(App app){
        this.app = app;
        createConnectElements();
    }

    private void createConnectElements(){
        HBox result = new HBox();
        Label addressLbl = new Label("BT-Address: ");
        TextField addressField = new TextField("98d331fb42b4");

        Button connBtn = new Button("Connect");
        Button disconnBtn = new Button("Disconnect");

        disconnBtn.setDisable(true);
        disconnBtn.setOnAction(e->disconnectFromDevice(connBtn, e));

        connBtn.setOnAction(e->connectToDevice(addressField.getText(), disconnBtn, e));

        result.setSpacing(10);
        result.setAlignment(Pos.CENTER_LEFT);
        result.setPadding(new Insets(10, 20, 10, 20));
        result.getChildren().addAll(addressLbl, addressField, connBtn, disconnBtn);
        getChildren().add(result);
    }

    private void connectToDevice(String address, Button dc, ActionEvent e){
        app.getLogger().appendText("Connecting to " + address + "!\n");

        if(!app.getBt().isConnected){
            try {
                app.getBt().connectToDevice(address);
                dc.setDisable(false);
                ((Button)e.getTarget()).setDisable(true);
            } catch (IOException e1) {
                app.getLogger().appendText("ERROR: " + e1.getMessage() + "\n");
            }
        }
    }

    private void disconnectFromDevice(Button conn, ActionEvent e){
        app.getLogger().appendText("Disconnecting!\n");
        if(app.getBt().isConnected){
            app.getBt().disconnect();
            conn.setDisable(false);
            ((Button)e.getSource()).setDisable(true);
        }
    }
}
