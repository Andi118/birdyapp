package de.thdeg.btapp.elements;

import de.thdeg.btapp.App;
import javafx.scene.control.TextArea;
import javafx.scene.layout.HBox;

public class LoggerElement extends HBox {

    private final App app;
    private final TextArea logOutput;

    public LoggerElement(App app, TextArea log){
        this.app = app;
        this.logOutput = log;
        log.setPrefWidth(600);

        getChildren().add(logOutput);
    }
}
