package de.thdeg.btapp.bluetooth;

import de.thdeg.btapp.elements.BTDevice;
import javafx.collections.ObservableList;
import javafx.scene.control.TextArea;

import javax.bluetooth.DiscoveryAgent;
import javax.bluetooth.LocalDevice;
import javax.bluetooth.RemoteDevice;
import javax.bluetooth.UUID;
import javax.microedition.io.Connector;
import javax.microedition.io.StreamConnection;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.util.ArrayList;
import java.util.List;

public class Bluetooth {

    protected static RemoteDevice device;
    public static String deviceURL;
    public static List<String> devices = new ArrayList<>();
    private ObservableList<BTDevice> btDevices;

    private UUID uuid = new UUID(0x1101);
    private UUID[] searchUUIDSet = new UUID[] {uuid};
    private int[] attrIDs = new int[] {0x0100};

    protected static final Object lock = new Object();

    private BTDiscoveryListener discoveryListener;
    private StreamConnection streamConnection;
    private OutputStream os;
    public InputStream is;

    private boolean discovering = false;

    public boolean isConnected = false;

    private Thread discoveryThread;
    private final TextArea output;

    public Bluetooth(TextArea output, ObservableList<BTDevice> devices) {
        this.btDevices = devices;
        this.output = output;
        discoveryListener = new BTDiscoveryListener(devices);
    }

    public void startDiscovery() {
        if(discovering){
            System.out.println("Discovery running...");

        } else {
            System.out.println("Start discovery!");
            discovering = true;
            btDevices.clear();
            discoveryThread = new Thread(() -> {
                try {
                    scanForDevices();
                    searchForServices();
                    discovering = false;

                } catch (IOException e) {
                    output.appendText("ERROR: " + e.getMessage() + "\n");
                    discovering = false;
                }
            });
            discoveryThread.start();
        }
    }

    private void scanForDevices() throws IOException {
        LocalDevice.getLocalDevice().getDiscoveryAgent().startInquiry(DiscoveryAgent.GIAC, discoveryListener);
        try {
            synchronized(lock) {
                lock.wait();
            }
        }catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    private void searchForServices() throws IOException {
        LocalDevice.getLocalDevice().getDiscoveryAgent().searchServices(attrIDs, searchUUIDSet, device, discoveryListener);
        try {
            synchronized(lock) {
                lock.wait();
            }
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        System.out.println("Hallo:" + deviceURL);
    }

    public void connectToDevice(String address) throws IOException {
        streamConnection = (StreamConnection) Connector.open("btspp://"+ address + ":1;authenticate=false;encrypt=false;master=false"); // Problem
        os = streamConnection.openOutputStream();
        is = streamConnection.openInputStream();
        isConnected = true;
    }

    public String read() {
        String message = "";
        try {
            while(is.available() != 0) {
                message = message + (char)is.read();
            }
            return message;
        } catch (IOException e) {
            e.printStackTrace();
            return "nope";
        }
    }

    public void readFloatArray(float[] result){
        byte[] bytes = new byte[result.length * 4];
        try{
            if(is.available() != 0) {
                int len = is.read(bytes, 0, 4 * result.length);
                System.out.println("LENGTH: " + len);
                for(int i = 0; i < result.length; i++){
                    result[i] = ByteBuffer.wrap(bytes, 4 * i, 4).order(ByteOrder.LITTLE_ENDIAN).getFloat();
                }

            }
        } catch (IOException e){
            e.getMessage();
        }
    }

    public float readFloat(){
        byte[] bytes = new byte[4];
        try {
            if(is.available() != 0)is.read(bytes, 0, 4);
            return ByteBuffer.wrap(bytes).order(ByteOrder.LITTLE_ENDIAN).getFloat();
        } catch (IOException e) {
            e.printStackTrace();
            return 0;
        }
    }

    public void write(byte[] command) throws IOException {
        os.write(command);
        os.flush();
    }

    public void disconnect() {
        if(streamConnection == null) return;
        try {
            os.close();
            is.close();
            streamConnection.close();
            isConnected = false;
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public List<String> getDivices(){ return devices; }
}
