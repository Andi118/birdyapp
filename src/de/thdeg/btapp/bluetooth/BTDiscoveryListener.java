package de.thdeg.btapp.bluetooth;

import de.thdeg.btapp.elements.BTDevice;
import javafx.collections.ObservableList;

import javax.bluetooth.DeviceClass;
import javax.bluetooth.DiscoveryListener;
import javax.bluetooth.RemoteDevice;
import javax.bluetooth.ServiceRecord;
import java.io.IOException;

public class BTDiscoveryListener implements DiscoveryListener {

    public ObservableList<BTDevice> devices;

    public BTDiscoveryListener(ObservableList<BTDevice> devices){
        this.devices = devices;
    }

    @Override
    public void deviceDiscovered(RemoteDevice btDevice, DeviceClass cod) {
//        try {
//            devices.add(new BTDevice(btDevice.getFriendlyName(false), btDevice.getBluetoothAddress()));
//        } catch(IOException e) {
//            e.printStackTrace();
//        }
    }

    @Override
    public void inquiryCompleted(int discType) {
        synchronized(Bluetooth.lock) {
            Bluetooth.lock.notify();
        }
    }

    @Override
    public void serviceSearchCompleted(int transID, int respCode) {
        synchronized(Bluetooth.lock) {
            Bluetooth.lock.notify();
        }
    }

    @Override
    public void servicesDiscovered(int arg0, ServiceRecord[] servRecord) {
        for(int i = 0; i < servRecord.length; i++) {
            Bluetooth.deviceURL = servRecord[i].getConnectionURL(ServiceRecord.NOAUTHENTICATE_NOENCRYPT, false);

            if(Bluetooth.deviceURL != null) {
                break;
            }
        }
    }
}
